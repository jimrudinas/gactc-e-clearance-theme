<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
 
 
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'liabilities',
	'meta_key'		=> 'liability_assigned_to',
	'meta_value'	=> get_current_user_id()
);

// query
$query = new WP_Query( $args );

// debug($query->posts);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_singular() ) : ?>
			<?php the_title( '<h1 class="entry-title default-max-width">', '</h1>' ); ?>
		<?php else : ?>
			<?php the_title( sprintf( '<h2 class="entry-title default-max-width"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<?php endif; ?>

		<?php twenty_twenty_one_post_thumbnail(); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="list-group">
		<?php foreach( $query->posts as $post ): 
		$clearance_id = get_field( 'liability_clearance', $post->ID );
		?>
			<a href="#" class="list-group-item list-group-item-action">
				<div class="d-flex w-100 justify-content-between">
					<h5 class="mb-1"><?php echo get_the_title($clearance_id); ?></h5>
					<small>3 days ago</small>
				</div>
				<p class="mb-1"><?php echo get_field( 'liability_description', $post->ID ); ?></p>
				<small>And some small placeholder content.</small>
			</a>
		<?php endForEach; ?>
		</div>
	</div>
	<!-- .entry-content -->

	<footer class="entry-footer default-max-width">
		<?php twenty_twenty_one_entry_meta_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
