<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
 
 
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'clearance',
	'author'		=> get_current_user_id(),
);

// query
$clearances = new WP_Query( $args );
// debug($clearances);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class();?>>
	<header class="entry-header">
		<?php if ( is_singular() ) : ?>
			<?php the_title( '<h1 class="entry-title default-max-width">', '</h1>' ); ?>
		<?php else : ?>
			<?php the_title( sprintf( '<h2 class="entry-title default-max-width"><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<?php endif; ?>

		<?php twenty_twenty_one_post_thumbnail(); ?>
	</header><!-- .entry-header -->
	<!-- Success Alert -->
	<div class="entry-content staff">
		
		<div class="clearance-title">
			<h1>Clearance List</h1>
			<!-- Button trigger modal -->
			<span><button 
					class="btn btn-primary has-background" 
					data-bs-toggle="modal" 
					data-bs-target="#staticBackdrop">
					Add New
			</button></span>
		</div>
		
		<?php if ( $clearances->posts ) : ?>
			<?php foreach($clearances->posts as $clearance) :
			$liabilities = get_related_liabilities( $clearance->ID );
			$count = 0;
			?>
			<div class="clearance-wrapper">
				<div class="clearance-title">
					<h2><?php echo $clearance->post_title; ?></h2>			
				</div>
				<?php if( $liabilities ) : ?>
					<div class="table-wrapper">
						<table class="table table-hover">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">First</th>
									<th scope="col">Last</th>
									<th scope="col">Status</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($liabilities->posts as $liability) : 
								
								$liability_fields = get_fields( $liability->ID );
								$student = get_user_by( 'id', $liability_fields['liability_assigned_to'][0] );
								$approve_display = $liability_fields['liability_status'] == 'Pending' ? 'show' : 'd-none';
								$decline_display = $liability_fields['liability_status'] == 'Cleared' ? 'show' : 'd-none';
								// debug($student);
								$count++;
								?>
								<tr>
									<th scope="row"><?php echo $count; ?></th>
									<td><?php echo $student->first_name ?></td>
									<td><?php echo $student->last_name ?></td>
									<td class="liability_status"><?php echo $liability_fields['liability_status']; ?></td>
									<td>
										<button 
											class="btn btn-success has-background approve <?php echo $approve_display; ?>" 
											value="Cleared"
											data-id="<?php echo $liability->ID;?>">Approve
										</button>
										<button 
											class="btn btn-danger has-background decline <?php echo $decline_display; ?>" 
											value="Pending"
											data-id="<?php echo $liability->ID;?>">Decline
										</button>
									</td>
								</tr>
								
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				<?php else : ?>
					<p>No Students Found.</p>
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
		<?php else: ?>
			<p>No Clearance Found.</p>
		<?php endif; ?>
	</div>
	<!-- .entry-content -->
	<?php 
		get_template_part( 'template-parts/custom/staff-modal' );
	?>
	<footer class="entry-footer default-max-width">
		<?php twenty_twenty_one_entry_meta_footer(); ?>
	</footer><!-- .entry-footer -->
	<div class="alert-container fixed-top"></div>
</article><!-- #post-<?php the_ID(); ?> -->
